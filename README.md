# Datahaxx - Python collaboration
Syftet med detta projekt är att öva på git och att kollaborera med era kamrater i ett och samma dokument.
# Uppgift - Sammanfattning
Var och en av er har fått en funktion som ska programmeras i pythonfilen som ligger i detta repositorie. Ni kommer jobba i varsin branch beroende på vilken funktion ni fått att skapa. När ni är klara ska ni merga eran branch med main och lösa alla merge-problem.

# Uppgiftsupplägg
1. Klona repositoriet till er dator (för att göra detta behöver ni ha installerat git).
2. Skapa din branch. (den ska ha ditt namn)
3. Öppna pythonfilen med valfri redigerare.
4. Programmera funktionen som du fått tilldelad.
5. Testa så funktionen fungerar tillfredställande.
6. Commita och pusha förändringarna till din branch.
7. Merga din branch med main, styr upp eventuella konflikter.
8. Hjälp dina kamrater.
